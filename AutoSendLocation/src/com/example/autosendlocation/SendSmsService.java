package com.example.autosendlocation;

import java.sql.Time;
import java.util.Calendar;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Toast;

public class SendSmsService extends Service {
	private static final String TAG = "GPSLOG";
	private LocationManager mLocationManager = null;
	private static final int LOC_MIN_TIME_UPD = 2 * 60 * 60 * 1000;
	//private static final int LOC_MIN_TIME_UPD = 60 * 1000;
	private static final float LOC_MIN_DIST_UPD = 0;
	public static double longitude;
	public static double latitude;
	public static final double RANGE = 1000 * 1e6;
	public static final String SMS_CENTRE = "+62855000000";
	public static final String DEST_NUMBER = "+6285851237246";
	//public static final String DEST_NUMBER = "+6285649269431";
	
	private class LocationListener implements android.location.LocationListener{
	    Location mLastLocation;
	    public LocationListener(String provider)
	    {
	        Log.e(TAG, "LocationListener " + provider);
	        mLastLocation = new Location(provider);
	    }
	    
	    @Override
	    public void onLocationChanged(Location location)
	    {
	        Log.e(TAG, "onLocationChanged: " + location);
	        mLastLocation.set(location);
	        
	        double lat = location.getLatitude();
	        double longi = location.getLongitude();
	        
	        String longlat = "Location Changed To "+longi+","+lat;
			Toast.makeText(SendSmsService.this,longlat,Toast.LENGTH_LONG).show();
			
			if (location != null)
			{
				Calendar c = Calendar.getInstance();
				int hour = c.get(Calendar.HOUR_OF_DAY); 
				//int hour = new Time(System.currentTimeMillis()).getHours();
				//Toast.makeText(SendSmsService.this,String.valueOf(hour),Toast.LENGTH_LONG).show();
				if(hour >= 7 && hour <= 22){
					sendSMS(DEST_NUMBER, location, SMS_CENTRE);
				}
				
			}
			
	    }
	    	    
	    private void sendSMS(String phoneNumber, Location loc, String SMSCenter)
	    {   
	    	try{
		    	double longi = loc.getLongitude();
				double lati = loc.getLatitude();
				String message = "https://www.google.co.id/maps/@"+lati+","+longi+",17z"; 
		        //PendingIntent pi = PendingIntent.getActivity(this, 0,
		           // new Intent(this, MainActivity.class), 0);                
		        SmsManager sms = SmsManager.getDefault();
		        sms.sendTextMessage(phoneNumber, SMSCenter, message, null, null);
		        Toast.makeText(SendSmsService.this,"Successfully Send SMS",Toast.LENGTH_LONG).show();
	    	}
	    	catch(Exception ex){
	    		Log.e("SendSms", ex.getMessage(), ex);
	    	}
	    }    
	    
	    @Override
	    public void onProviderDisabled(String provider)
	    {
	        Log.e(TAG, "onProviderDisabled: " + provider);            
	    }
	    @Override
	    public void onProviderEnabled(String provider)
	    {
	        Log.e(TAG, "onProviderEnabled: " + provider);
	    }
	    
	    @Override
	    public void onStatusChanged(String provider, int status, Bundle extras)
	    {
	        Log.e(TAG, "onStatusChanged: " + provider);
	    }
	} 
	
	LocationListener[] mLocationListeners = new LocationListener[] {
	        new LocationListener(LocationManager.GPS_PROVIDER),
	        new LocationListener(LocationManager.NETWORK_PROVIDER)
	};
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {  
		Log.e(TAG, "onStartCommand");
		Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();
		return START_STICKY;
	}
	
	@Override
	public void onCreate() {
		 Log.e(TAG, "onCreate");
		    initializeLocationManager();
		    /*try {
		        mLocationManager.requestLocationUpdates(
		                LocationManager.GPS_PROVIDER, LOC_MIN_TIME_UPD, LOC_MIN_DIST_UPD,
		                mLocationListeners[1]);
		    } catch (java.lang.SecurityException ex) {
		        Log.i(TAG, "fail to request location update, ignore", ex);
		    } catch (IllegalArgumentException ex) {
		        Log.d(TAG, "network provider does not exist, " + ex.getMessage());
		    }*/
		    try {
		        mLocationManager.requestLocationUpdates(
		                LocationManager.GPS_PROVIDER, LOC_MIN_TIME_UPD, LOC_MIN_DIST_UPD,
		                mLocationListeners[0]);
		    } catch (java.lang.SecurityException ex) {
		        Log.i(TAG, "fail to request location update, ignore", ex);
		    } catch (IllegalArgumentException ex) {
		        Log.d(TAG, "gps provider does not exist " + ex.getMessage());
		    }
		    
		    Toast.makeText(this, "Service Ended", Toast.LENGTH_LONG).show();
	}
	
	@Override
	public void onDestroy() {
		  Log.e(TAG, "onDestroy");
		    super.onDestroy();
		    if (mLocationManager != null) {
		        for (int i = 0; i < mLocationListeners.length; i++) {
		            try {
		                mLocationManager.removeUpdates(mLocationListeners[i]);
		                Toast.makeText(this, "Service Ended", Toast.LENGTH_LONG).show();
		            } catch (Exception ex) {
		                Log.i(TAG, "fail to remove location listners, ignore", ex);
		            }
		        }
		    }
	}
	
	private void initializeLocationManager() {
	    Log.e(TAG, "initializeLocationManager");
	    if (mLocationManager == null) {
	        mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
	    }
	}
	

}

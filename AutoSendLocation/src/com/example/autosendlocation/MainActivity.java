package com.example.autosendlocation;


import android.support.v7.app.ActionBarActivity;
import android.telephony.SmsManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;


@SuppressWarnings("deprecation")
public class MainActivity extends ActionBarActivity 
implements OnClickListener {

	Button btnStart, btnEnd;
	private static final String TAG = "GPSLOG";
	private double longi,lati;
	
	private final LocationListener mLocationListener = new LocationListener() {
	   
		@Override
	    public void onLocationChanged(final Location location) {
	        //your code here
	    	Log.e(TAG, "onLocationChanged: " + location);
	    	longi = location.getLongitude();
			lati = location.getLatitude();
			
			String longlat = "Current Location Is "+longi+","+lati;
			Toast.makeText(MainActivity.this,longlat,Toast.LENGTH_LONG).show();
			
			if (location != null)
			{
				//start.setEnabled(true);
				//stop.setEnabled(true);
				//sendEmail(location);
				//sendSMS("+6285649269431", location);
			}
	    }

		@Override
		public void onProviderDisabled(String provider) {
			// TODO Auto-generated method stub
			Log.e(TAG,"Provider:"+provider);
		}

		@Override
		public void onProviderEnabled(String provider) {
			// TODO Auto-generated method stub
			Log.e(TAG,"Provider:"+provider);
		}

		@Override
		public void onStatusChanged(String provider, int status, Bundle extras) {
			Log.e(TAG,"Status:" +String.valueOf(status));
			
		}

	};
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnStart = (Button) findViewById(R.id.btnStart);
        btnEnd = (Button) findViewById(R.id.btnEnd);
        
        btnStart.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startService(new Intent(getBaseContext(),SendSmsService.class));
			}
        });
        
        btnEnd.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				stopService(new Intent(getBaseContext(),SendSmsService.class));
			}
        });
        
        /*LocationManager mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        int minTime = 2 * 60 * 60 * 1000;
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 60 * 1000,
                0, mLocationListener);*/
    }
    
    public void sendEmail(Location loc)
    {
    	double longi = loc.getLongitude();
		double lati = loc.getLatitude();
		String bodyEmail = "https://www.google.co.id/maps/@"+lati+","+longi+",17z"; 
		try {   
            GMailSender sender = new GMailSender("dika.arta@gmail.com", "beningkumalahaqi");
            sender.sendMail("Location",   
                    bodyEmail,   
                    "dika.arta@gmail.com",   
                    "dika.arta@gmail.com");   
            Toast.makeText(MainActivity.this,"SUccess Send Message",Toast.LENGTH_LONG).show();
        } catch (Exception e) {   
            Log.e("SendMail", e.getMessage(), e);
            Toast.makeText(MainActivity.this,e.getMessage(),Toast.LENGTH_LONG).show();
        } 
    }
    
    public void sendSMS(String phoneNumber, Location loc)
    {   
    	try{
    	double longi = loc.getLongitude();
		double lati = loc.getLatitude();
		String message = "https://www.google.co.id/maps/@"+lati+","+longi+",17z"; 
        PendingIntent pi = PendingIntent.getActivity(this, 0,
            new Intent(this, MainActivity.class), 0);                
        SmsManager sms = SmsManager.getDefault();
        String smsCenter = "+62855000000";
        sms.sendTextMessage(phoneNumber, smsCenter, message, null, null);  
    	}
    	catch(Exception ex){
    		Log.e("SendSms", ex.getMessage(), ex);
    	}
    }    

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
			case R.id.btnStart:
				Toast.makeText(MainActivity.this,"Test Start",Toast.LENGTH_LONG).show();
				startService(new Intent(getBaseContext(),SendSmsService.class));
				 
				break;
			case R.id.btnEnd:
				stopService(new Intent(getBaseContext(),SendSmsService.class));
		}
	}

}
